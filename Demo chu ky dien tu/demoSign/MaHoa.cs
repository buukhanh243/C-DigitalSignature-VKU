﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MD5;

namespace demoSign
{
    public partial class MaHoa : Form
    {
        public MaHoa()
        {
            InitializeComponent();
        }

        private void btEncrypt_Click(object sender, EventArgs e)
        {
            Encrypt encrypt = new Encrypt();
            if (txtChuoiBanDau.Text.Trim() == "")
            {
                MessageBox.Show("Mời bạn nhập chuỗi");
                return;
            }
            if (rdMd5.Checked != true && rdSHA1.Checked != true)        
            {
                MessageBox.Show("Mời bạn chọn kiểu mã hóa");
                return;
            }
            try
            {
                int a = Convert.ToInt32(txtSolan.Text.Trim());
                if (a == 0)
                {
                    MessageBox.Show("Số lần là số tự nhiên lớn hơn 0");
                }
                else
                {
                    if(rdMd5.Checked == true)
                    {
                        txtKetQua.Text = encrypt.MD52(txtChuoiBanDau.Text, a);
                    }
                    if(rdSHA1.Checked == true)
                    {
                        txtKetQua.Text = encrypt.SHAn(txtChuoiBanDau.Text,a);
                    }
                }
            }
            catch
            {
                MessageBox.Show("Số lần là số tự nhiên lớn hơn 0");
            }
           
            
            
        }

        private void rdMd5_CheckedChanged(object sender, EventArgs e)
        {
            label4.Visible = true;
            txtSolan.Visible = true;
        }

        private void rdSHA1_CheckedChanged(object sender, EventArgs e)
        {
            label4.Visible = true;
            txtSolan.Visible = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MaHoa_Load(object sender, EventArgs e)
        {

        }

        private void txtKetQua_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }
    }
}
